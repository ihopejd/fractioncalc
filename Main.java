import java.util.Scanner;

    public class Main {
        static Scanner scanner = new Scanner(System.in);
        public static void main(String args[]){

            double firstDenominator = 0, firstNuminator = 0, secondDenominator = 0, secondNuminator = 0; //Иниөиализаөия переменных для дробей
            char optionForMethod; //Переменная для знака операции

            for(int i = 1; i < 5; i++){

                switch(i){
                    case 1:
                        firstDenominator = scanner.nextDouble();
                        break;
                    case 2:
                        firstNuminator = scanner.nextDouble();
                        break;
                    case 3:
                        secondDenominator = scanner.nextDouble();
                        break;
                    case 4:
                        secondNuminator = scanner.nextDouble();
                        break;


                }
            }


            Fraction fractionFirst = new Fraction(firstDenominator, firstNuminator); //Ввод значений переменных в поля объекта типа Fraction. Первая дробь.
            Fraction fractionSecond = new Fraction(secondDenominator, secondNuminator);  //Ввод значений переменных в поля объекта типа Fraction. Вторая дробь.
	/*Метод для вывода дробей на экран. Выводит первую и вторую дробь, используя
	* методы из класса Fraction для каждой дроби: первой и второй.
	*/
            fractionVisible(fractionFirst, fractionSecond);
            System.out.println("What would you do with this fractions?");
            System.out.println("You can do '+' '-' '/' '*' with fractions. Choose one: ");
            optionForMethod = scanner.next().charAt(0); //Ввод данных типа Char для операций с дробями, чем и занимается оператор switch
            switch(optionForMethod){
                case '+':
                    Method.outPrint()
                    break;
                case '-':
                    Method.subtraction(fractionFirst, fractionSecond);
                    break;
                case '/':
                    Method.division(fractionFirst, fractionSecond);
                    break;
                case '*':
                    Method.multiplication(fractionFirst, fractionSecond);
                    break;


            }
            System.out.println("Result is: " + Method.outPrint());

        }

        public static void fractionVisible(Fraction fractionFirst, Fraction fractionSecond){
            System.out.println("First fraction is: ");
            System.out.println(Fraction.outPrint(fractionFirst));
            System.out.println("Second fraction is: ");
            System.out.println(Fraction.outPrint(fractionSecond));
        }


    }