public class Method {

    private static Fraction reducingDenominatorFirst(Fraction fractionFirst, Fraction fractionSecond){
        Fraction equalDenominatorFirst = new Fraction(fractionFirst.getNumenator() * fractionSecond.getDenominator(), fractionFirst.getDenominator() * fractionSecond.getNumenator());
        return equalDenominatorFirst;
    }

    private static Fraction reducingDenominatorSecond(Fraction fractionFirst, Fraction fractionSecond){
        Fraction equalDenominatorSecond = new Fraction(fractionSecond.getNumenator() * fractionFirst.getDenominator(), fractionSecond.getDenominator() * fractionFirst.getNumenator());
        return equalDenominatorSecond;
    }


    public static Fraction addition(Fraction fractionFirst, Fraction fractionSecond){
        if(fractionFirst.getDenominator() == fractionSecond.getDenominator()){
            System.out.println("Denominators are equal.");
        } else {
            System.out.println("Reducing to the global denominator...");
        }
        Fraction fractionResult = new Fraction(reducingDenominatorFirst(fractionFirst, fractionSecond).getNumenator() + reducingDenominatorSecond(fractionFirst, fractionSecond).getNumenator(), reducingDenominatorSecond(fractionFirst, fractionSecond).getDenominator());
        return fractionResult;
    }

    public static Fraction subtraction(Fraction fractionFirst, Fraction fractionSecond){
        if(fractionFirst.getDenominator() == fractionSecond.getDenominator()){
            System.out.println("Denominators are equal.");
        } else {
            System.out.println("Reducing to the global denominator...");
        }
        Fraction fractionResult = new Fraction(reducingDenominatorFirst(fractionFirst, fractionSecond).getNumenator() - reducingDenominatorSecond(fractionFirst, fractionSecond).getNumenator(), reducingDenominatorSecond(fractionFirst, fractionSecond).getDenominator());
        return fractionResult;
    }

    public static Fraction division(Fraction fractionFirst, Fraction fractionSecond){
        Fraction.coup(fractionSecond); //Переворот второй дроби
        Fraction fractionResult = new Fraction(fractionFirst.getNumenator() * fractionSecond.getNumenator(), fractionFirst.getDenominator() * fractionSecond.getDenominator());
        return fractionResult;
    }
    
    public static Fraction multiplication(Fraction fractionFirst, Fraction fractionSecond) {
        Fraction fractionResult = new Fraction(fractionFirst.getNumenator() * fractionSecond.getNumenator(), fractionFirst.getDenominator() * fractionSecond.getDenominator());
        return fractionResult;
    }

    public static Fraction coup(Fraction fraction) {
        double saveNumenator = fraction.getNumenator();
        double saveDenominator = fraction.getDenominator();
        fraction.getDenominator(saveDenominator);
        fraction.getDenominator() = saveNumenator;
        return fraction;    
    }


    public void outPrint(Fraction fraction) {
        System.out.println(fraction.numenator + "/" + fraction.denominator);
}